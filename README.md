# README #

### What is this repository for? ###

This repo is where we store scripts for enhancing productivity in the LabVIEW IDE.

### Scripts ###

Name | LabVIEW Version | Owner
---- |-----------------|-------
__Add License to VI Documentation__ | 2015 | Ethan Stern
__Add to Private Data__ | 2015 | Ethan Stern
__Set All Panes to Origins__ | 2015 | Ethan Stern
__Size Window to Contents__ | 2015 | Ethan Stern

### How do I get set up? ###

To install a downloaded script, place it and its dependencies folder (if it has one) into one of these locations:

* _[LabVIEW]_\resource\dialog\QuickDrop\plugins
* _[LabVIEW Data]_\Quick Drop plugins

### Contribution guidelines ###

* Please reach out if you would like to commit changes.
* Please do not up-rev LabVIEW versions of scripts.
* Scripts should not require any wacky dependencies.

### Who do I talk to? ###

* [Ethan Stern](mailto:ethan.stern@composed.io)