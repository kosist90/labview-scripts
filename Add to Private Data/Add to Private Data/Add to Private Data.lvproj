﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Test" Type="Folder">
			<Item Name="MyClass.lvclass" Type="LVClass" URL="../Test/MyClass/MyClass.lvclass"/>
			<Item Name="MyActor.lvlib" Type="Library" URL="../Test/MyActor/MyActor.lvlib"/>
			<Item Name="Test Unclaimed VI.vi" Type="VI" URL="../Test/Test Unclaimed VI.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Add Reference Control to Cluster and Rename.vi" Type="VI" URL="../Add Reference Control to Cluster and Rename.vi"/>
			<Item Name="Add Value Control to Cluster and Rename.vi" Type="VI" URL="../Add Value Control to Cluster and Rename.vi"/>
			<Item Name="Bundle Actor Core Refs.vi" Type="VI" URL="../Bundle Actor Core Refs.vi"/>
			<Item Name="Check Is Actor Core.vi" Type="VI" URL="../Check Is Actor Core.vi"/>
			<Item Name="Clean Up References and Bundle Node.vi" Type="VI" URL="../Clean Up References and Bundle Node.vi"/>
			<Item Name="Close Private Data Cluster Refs.vi" Type="VI" URL="../Close Private Data Cluster Refs.vi"/>
			<Item Name="Copy From Diagram to Private Data.vi" Type="VI" URL="../Copy From Diagram to Private Data.vi"/>
			<Item Name="Copy From Panel to Private Data.vi" Type="VI" URL="../Copy From Panel to Private Data.vi"/>
			<Item Name="Move Label to Left.vi" Type="VI" URL="../Move Label to Left.vi"/>
			<Item Name="Open Actor ControlTerminal Ref.vi" Type="VI" URL="../Open Actor ControlTerminal Ref.vi"/>
			<Item Name="Open Actor Core Input Terminal Ref.vi" Type="VI" URL="../Open Actor Core Input Terminal Ref.vi"/>
			<Item Name="Open Private Data Cluster Refs.vi" Type="VI" URL="../Open Private Data Cluster Refs.vi"/>
			<Item Name="Prepare Diagram GObject for Move.vi" Type="VI" URL="../Prepare Diagram GObject for Move.vi"/>
			<Item Name="Prepare Panel GObject for Move.vi" Type="VI" URL="../Prepare Panel GObject for Move.vi"/>
			<Item Name="Private Data Refs.ctl" Type="VI" URL="../Private Data Refs.ctl"/>
			<Item Name="Resize Private Data Panel.vi" Type="VI" URL="../Resize Private Data Panel.vi"/>
			<Item Name="Wire Bundle By Name Node.vi" Type="VI" URL="../Wire Bundle By Name Node.vi"/>
			<Item Name="Wire ControlReferenceConstants.vi" Type="VI" URL="../Wire ControlReferenceConstants.vi"/>
		</Item>
		<Item Name="Add to Private Data.vi" Type="VI" URL="../../Add to Private Data.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="QuickDrop Parse Plugin Variant.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/QuickDropSupport/QuickDrop Parse Plugin Variant.vi"/>
				<Item Name="QuickDrop Plugin Data ver1.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/QuickDropSupport/QuickDrop Plugin Data ver1.ctl"/>
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="C2Demo" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{C0DEE908-FA6B-49AF-8E98-8534AC2DD466}</Property>
				<Property Name="App_INI_GUID" Type="Str">{DCAA2EC3-928D-48AA-B94F-BC74F2C2E3A4}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{89F95312-4F40-4E83-8799-DFC008AAAF7C}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">C2Demo</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/C2Demo</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{0D7C63DA-177B-44A7-848F-CF9ABE491725}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CoolScript.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/C2Demo/CoolScript.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/C2Demo/data</Property>
				<Property Name="Destination[2].destName" Type="Str">Configuration</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/NI_AB_PROJECTNAME/C2Demo/Configuration</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].itemID" Type="Str">{333168D8-7979-4875-B503-BF9C4595A72C}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">Composed Systems</Property>
				<Property Name="TgtF_fileDescription" Type="Str">C2Demo</Property>
				<Property Name="TgtF_internalName" Type="Str">C2Demo</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 Composed Systems</Property>
				<Property Name="TgtF_productName" Type="Str">C2Demo</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{841AE6BE-C31D-4042-B57C-5247CEFD6458}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CoolScript.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
